Pygame OpenGL lens

This little project shows the use of shaders, textures and the mouse in Python with Pygame.

Start the demonstration by running 'pygamelens.py'. You should see the image clear where the mouse is, and noisy where the mouse is not.

Start playing around by changing the bottom lines of 'lensshader.py'. This is where the backgroun d image and the foreground noise are mixed. 