#!/usr/bin/env python3
'''
Copyright © 2013-2021, W. van Ham, Radboud University Nijmegen
This file is part of Sleelab.

Sleelab is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sleelab is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sleelab.  If not, see <http://www.gnu.org/licenses/>.
'''

import sys
import re
from OpenGL.GL import *
from OpenGL.GL import shaders

def initializeShaders(vertexShaderString, fragmentShaderString, geometryShaderString=None):
	if not glUseProgram:
		print ('Missing Shader Objects!')
		exit(1)
	try:
		vertexShader = shaders.compileShader(vertexShaderString, GL_VERTEX_SHADER)
		if geometryShaderString !=None:
			geometryShader = shaders.compileShader(geometryShaderString, GL_GEOMETRY_SHADER)
		fragmentShader = shaders.compileShader(fragmentShaderString, GL_FRAGMENT_SHADER)
	except RuntimeError as errorString:
		print ("ERROR: Shaders did not compile properly:", type(errorString))
		m = re.search('^(.*?)\[b\'(.*)\'\]', str(errorString))
		if m:
			print(m.group(1))
			a = m.group(2)
		a = (a).split('\\n')
		for i in range(0,len(a)):
			print ("{0:3d} {1:}".format(i+1, a[i]))
		sys.exit(1)
	try:
		if geometryShaderString !=None:
			program = shaders.compileProgram(vertexShader, fragmentShader, geometryShader)
		else:
			program = shaders.compileProgram(vertexShader, fragmentShader)
	except RuntimeError as errorString:
		print("ERROR: Shader program did not link/validate properly: {}".format(errorString))
		sys.exit(1)
	
	glUseProgram(program)
	return program

vs = """#version 430
layout(location = 0) in vec2 position; // vertex coordinate, location 0 is default
out vec2 texPosition; 

void main() {
	gl_Position = vec4(position[0],position[1],0, 1.0); // 2D to 3D
	texPosition = vec2(.5,.5)+0.5*position; // clip space (-1 -- +1) to texture space (0 -- 1)
}
"""


fs = """#version 430
in vec2 texPosition;                              // texture coordinates 0-1 from vertex shader
layout(location = 0) out vec4 fragColor;          // output color
layout(binding = 0) uniform sampler2D background; // texture unit 0, background image
layout(binding = 1) uniform sampler2D foreground; // texture unit 1, foreground noise
layout(location = 0) uniform ivec2 window;        // window size in pixels
layout(location = 1) uniform ivec2 mouse;         // mouse position in pixels
const float pi = radians(180);

void main() {
	// d is distance from mouse cursor to fragment in units of diagonal
	float d = distance(vec2(gl_FragCoord.x, gl_FragCoord.y), mouse) / length(window);
	if(d<0.1) {
		float fraction = 0.3 + 0.7*(0.5 * (1.0 + cos(d/0.1 * pi))); // 0.3 - 1.0
		fragColor = fraction * texture(background, texPosition) + (1.0-fraction) * texture(foreground, texPosition);
		//fragColor = texture(background, texPosition);
	} else {
		fragColor = 0.3 * texture(background, texPosition) + 0.7 * texture(foreground, texPosition);
	}
}
"""
