#!/usr/bin/env python3
import numpy as np
from OpenGL import GL
from PIL import Image
from psychopy import visual, event, core

## constants
windowSize = (800, 600)
textureSize = 1024 # power of 2 larger than largest window dimension
apertureSize = 150 # size of hole in noise

## open a window to render the shape
win = visual.Window(windowSize, winType="pyglet", monitor='testMonitor', units="pix", allowStencil=True)
mouse = event.Mouse(True)

## make stimuli
message = visual.TextStim(win, text='Press any key to quit', pos=(0, -250))
image = visual.ImageStim(win, size=windowSize, image="sky.png", units="pix")
# noise image stimulus
noise = visual.ImageStim(win, size=windowSize, units="pix")
with Image.open("sky.png") as pimage:
	pimage = pimage.transpose(Image.FLIP_TOP_BOTTOM).resize((textureSize, textureSize), Image.NEAREST) # resize
	imageData = np.reshape(np.array(pimage.getdata(), np.float32)/255, (textureSize, textureSize, 3)) # cast
noiseData = np.random.uniform(0, 1, size=(textureSize, textureSize, 3)).astype(np.float32)
noise.image = (imageData + noiseData)/2
# aperture for use with image without noise
aperture = visual.Aperture(win, size=apertureSize, shape="circle")
aperture.enabled = False

## event loop
while not event.getKeys():
	# noisy image
	noise.draw()
	# image without noise in aperture
	aperture.enabled = True
	aperture.pos = mouse.getPos() # center aperture on mouse position
	image.draw()
	aperture.enabled = False
	# text
	message.draw()
	win.flip()
