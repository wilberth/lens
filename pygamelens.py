#!/usr/bin/env python3
import ctypes
import numpy as np
import pygame
from OpenGL.GL import *
from PIL import Image
import lensshader as shader
import time

# load or create data for foreground and background images
# background image rgb
with Image.open("sky.png").transpose(Image.FLIP_TOP_BOTTOM) as image:
	backgroundData = np.array(image.getdata(), np.uint8)
	width, height = image.size
# foreground noise, grayscale
foregroundData = np.random.randint(0, 256, size=(width*height), dtype='uint8')

# pygame, initialize window
pygame.init()
flags = pygame.OPENGL#|pygame.DOUBLEBUF|pygame.FULLSCREEN|pygame.HWSURFACE
screen = pygame.display.set_mode((width, height), flags)

# set up vertex buffer with two triangles covering whole screen
glEnableClientState(GL_VERTEX_ARRAY) # drawArrays will draw vertex array
vertices = [ -1.0,1.0,  -1.0,-1.0,  1.0,1.0, 1.0,-1.0 ] # two triangles, forming a rectangular triangle strip
vbo = glGenBuffers(1)
glBindBuffer(GL_ARRAY_BUFFER, vbo)
glBufferData(GL_ARRAY_BUFFER, len(vertices)*4, (ctypes.c_float*len(vertices))(*vertices), GL_STATIC_DRAW)
glVertexPointer(2, GL_FLOAT, 0, None) # just x,y (2 coordinates)

# compile and use the shaders
program = shader.initializeShaders(shader.vs, shader.fs)

# write uniforms
glUniform2i(0, width, height) # 0 is hard coded location of 'window' in vertex shader

# set up background texture
background = glGenTextures(1)
glBindTexture(GL_TEXTURE_2D, background) # bind active texture unit (default 0) to texture name value
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0) # mipmap level, default 1000
glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, backgroundData)

# set up foreground texture
foreground = glGenTextures(1)
glActiveTexture(GL_TEXTURE1) # texture unit 1
glBindTexture(GL_TEXTURE_2D, foreground)
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0) 
glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, width, height, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, foregroundData)

t0 = t1 = time.time()
n = 0
while True:
	for event in pygame.event.get():
		if event.type == pygame.QUIT or event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
			exit()

	# tell gpu about mouse position
	x, y = pygame.mouse.get_pos()
	glUniform2i(1, x, height-y) # 1 is hard coded location of 'mouse' in vertex shader
	
	# draw image
	glDrawArrays (GL_TRIANGLE_STRIP, 0, 4)

	pygame.display.flip()
	n+=1
	if not n%100:
		t0 = t1
		t1 = time.time()
		print("{:6.1f} Hz".format(100/(t1-t0)), end="\r")
