#!/usr/bin/env python3
'''
Copyright © 2013-2021, W. van Ham, Radboud University Nijmegen
This file is part of Sleelab.

Sleelab is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sleelab is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sleelab.  If not, see <http://www.gnu.org/licenses/>.
'''

import sys
import re
import numpy as np
from OpenGL import GL
from OpenGL.GL import *
from OpenGL.GL import shaders
from PIL import Image
from psychopy import core
from psychopy import visual
from psychopy.visual import PlaneStim, BlinnPhongMaterial, Window, shaders as _shaders
from psychopy.tools.gltools import createTexImage2dFromFile, createTexImage2D
import psychopy.tools.mathtools as mt
import psychopy.tools.gltools as gt
import psychopy.tools.arraytools as at
import psychopy.tools.viewtools as vt

# local draw
import psychopy.tools.gltools as gltools
from ctypes import c_int, c_char_p, c_char, cast, POINTER, byref




def initializeShaders(vertexShaderString, fragmentShaderString, geometryShaderString=None):
	if not glUseProgram:
		print ('Missing Shader Objects!')
		exit(1)
	try:
		vertexShader = shaders.compileShader(vertexShaderString, GL_VERTEX_SHADER)
		if geometryShaderString !=None:
			geometryShader = shaders.compileShader(geometryShaderString, GL_GEOMETRY_SHADER)
		fragmentShader = shaders.compileShader(fragmentShaderString, GL_FRAGMENT_SHADER)
	except RuntimeError as errorString:
		print ("ERROR: Shaders did not compile properly:", type(errorString))
		m = re.search('^(.*?)\[b\'(.*)\'\]', str(errorString))
		if m:
			print(m.group(1))
			a = m.group(2)
		a = (a).split('\\n')
		for i in range(0,len(a)):
			print ("{0:3d} {1:}".format(i+1, a[i]))
		sys.exit(1)
	try:
		if geometryShaderString !=None:
			program = shaders.compileProgram(vertexShader, fragmentShader, geometryShader)
		else:
			program = shaders.compileProgram(vertexShader, fragmentShader)
	except RuntimeError as errorString:
		print("ERROR: Shader program did not link/validate properly: {}".format(errorString))
		sys.exit(1)
	
	glUseProgram(program)
	return program

class LensStim():
	def __init__(self, win, backgroundFile):
		self._win = win
		self.program = initializeShaders(vs, fs)
		GL.glUniform2i(0, win.size[0], win.size[1]) # window size
		with Image.open(backgroundFile).transpose(Image.FLIP_TOP_BOTTOM) as image:
			self.width, height = image.size
			
		# create the stimulus object
		self.planeStim = PlaneStim(self._win, size=(2,2))
		self.backgroundTexture = createTexImage2dFromFile(backgroundFile) # load a diffuse texture
		
		width, height = self._win.size
		foregroundData = np.random.randint(0, 256, size=(width, height, 3), dtype='uint8')
		"""
		self.foregroundTexture = glGenTextures(1)
		glActiveTexture(GL_TEXTURE3) # texture unit 1
		glBindTexture(GL_TEXTURE_2D, self.foregroundTexture)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0) 
		glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, self._win.size[0], self._win.size[1], 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, foregroundData)
		"""
		self.foregroundTexture = createTexImage2D(
			self._win.size[0],
			self._win.size[1],
			internalFormat=GL.GL_RGB,
			pixelFormat=GL.GL_RGB,
			dataType=GL.GL_UNSIGNED_BYTE,
			data=foregroundData.ctypes,
			unpackAlignment=1,
			texParams={GL.GL_TEXTURE_MAG_FILTER: GL.GL_LINEAR, GL.GL_TEXTURE_MIN_FILTER: GL.GL_LINEAR})

	def draw(self, x, y):
		# setup textures

		gt.bindTexture(self.backgroundTexture, 0)
		gt.bindTexture(self.foregroundTexture, 1)
		GL.glUseProgram(self.program)
		GL.glUniform2i(1, round((x+1)*self._win.size[0]/2), round((y+1)*self._win.size[1]/2)) # 1 is hard coded location of 'mouse' in vertex shader
		# replace next line
		gt.drawVAO(self.planeStim._vao, GL.GL_TRIANGLES)
		#vao = self.planeStim._vao
		#GL.glBindVertexArray(vao.name)
		#GL.glDrawElements(GL.GL_TRIANGLES, vao.count, vao.indexBuffer.dataType, 0)
		#GL.glDrawArrays(GL.GL_TRIANGLES, start, count)
		# reset
		GL.glBindVertexArray(0)


		gt.unbindTexture(self.backgroundTexture)
		#gt.unbindTexture(self.foregroundTexture) # do this and we only see the foreground
		#GL.glDisable(GL.GL_TEXTURE_2D)

		self._win.resetEyeTransform()  # reset the transformation to draw 2D stimuli


vs = """#version 430
layout(location = 0) in vec2 position; // vertex coordinate, location 0 is default
out vec2 texPosition; 

void main() {
	gl_Position = vec4(position[0],position[1],0, 1.0); // 2D to 3D
	texPosition = vec2(.5,.5)+0.5*position; // clip space (-1 -- +1) to texture space (0 -- 1)
}
"""


fs = """#version 430
in vec2 texPosition;                              // texture coordinates 0-1 from vertex shader
layout(location = 0) out vec4 fragColor;          // output color
layout(binding = 0) uniform sampler2D background; // texture unit 0, background image
layout(binding = 1) uniform sampler2D foreground; // texture unit 3, foreground noise
layout(location = 0) uniform ivec2 window;        // window size in pixels
layout(location = 1) uniform ivec2 mouse;         // mouse position in pixels
const float pi = radians(180);

void main() {
	// d is distance from mouse cursor to fragment in units of diagonal
	float d = distance(vec2(gl_FragCoord.x, gl_FragCoord.y), mouse) / length(window);
	if(d<0.1) {
		fragColor=vec4(0,0,0,0);
		//fragColor = texture(foreground, texPosition);
	} else {
		fragColor = texture(background, texPosition);
	}
}
"""
